FROM ruby:2.6.5

RUN apt-get update && apt-get install -y nodejs && apt-get install -y npm

RUN npm install -g yarn

RUN mkdir /alitwitter
WORKDIR /alitwitter

COPY Gemfile /alitwitter/Gemfile
COPY Gemfile.lock /alitwitter/Gemfile.lock
RUN gem install bundler -v "2.1.4"
RUN bundle install

COPY package.json /alitwitter
COPY yarn.lock /alitwitter
RUN yarn --pure-lockfile

COPY . /alitwitter

RUN yarn install --check-files

ARG ALITWITTER_DATABASE_USERNAME=bootcamp_alitwitter
ENV ALITWITTER_DATABASE_USERNAME="${ALITWITTER_DATABASE_USERNAME}"
ARG ALITWITTER_DATABASE_PASSWORD=bootcamp_alitwitter_password
ENV ALITWITTER_DATABASE_PASSWORD="${ALITWITTER_DATABASE_PASSWORD}"
ARG ALITWITTER_DATABASE_IP=192.168.88.11
ENV ALITWITTER_DATABASE_IP="${ALITWITTER_DATABASE_IP}"
ARG ALITWITTER_DATABASE_PORT=5432
ENV ALITWITTER_DATABASE_PORT="${ALITWITTER_DATABASE_PORT}"
ARG ALITWITTER_DEVELOPMENT_DB_NAME=alitwitter_development
ENV ALITWITTER_DEVELOPMENT_DB_NAME="${ALITWITTER_DEVELOPMENT_DB_NAME}"
ARG ALITWITTER_TEST_DB_NAME=alitwitter_test
ENV ALITWITTER_TEST_DB_NAME="${ALITWITTER_TEST_DB_NAME}"

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]
