## Description
A Twitter like web application

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

Install all dependency using bundle in root of directory project.
```
bundle install
```

## Run Test
using rspec & rubocop
```bash
bundle exec rake
```

to open the coverage result in browser
```bash
bundle exec rake coverage
```

## How to run locally

Please run this before starting the server

```
yarn
rake db:setup
rake db:migrate
```

To run local server, please use

```
rails server
```

This will open up a service in port 3000, open http://127.0.0.1:3000 in your browser to start using the application

## How to deploy

There is a Dockerfile provided, internal service is running at port 3000. Make sure you have docker installed and after that to deploy you can use this command:

```
docker build -t bootcamp_alitwitter .
docker run -t -p 3000:3000 bootcamp_alitwitter
```

And open http://127.0.0.1:3000 in your browser

## How to deploy in Vagrant

There is a Vagrantfile provided, please have your docker images as tar files using docker save command, for example

```
docker save bootcamp_alitwitter > bootcamp_alitwitter.tar
```

After that open provision/group_vars/all.yaml and edit config inside there to suit your need, for example

```
---
artifact:
  path: /Users/saka.putra/Development/Bootcamp/DevOps/PostgreSQLPlayground/PostgresPlay
  name: bootcamp_alitwitter.tar
docker:
  image_name: bootcamp_alitwitter
  port:
    host: 3000
    container: 3000
database:
  username: bootcamp_alitwitter
  password: bootcamp_alitwitter_password
  ip: 192.168.88.11
  port: 5432
```

Then turn on vagrant server by using

```
vagrant up
```

Next please provision the server by running

```
ansible-playbook provision/bootcamp_alitwitter_deploy.yml -i provision/bootcamp_alitwitter_hosts.yml
```

That will setup a DB and APP server.

After that you can open http://127.0.0.1:3000 in your browser to use the application

If you want to redeploy, please update the artifact and run

```
ansible-playbook provision/bootcamp_alitwitter_deploy.yml -i provision/bootcamp_alitwitter_hosts.yml
```

If you want to undeploy please run

```
ansible-playbook provision/bootcamp_alitwitter_undeploy.yml -i provision/bootcamp_alitwitter_hosts.yml
```
