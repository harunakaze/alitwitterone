class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all.sort_by(&:created_at).reverse
    @tweet = Tweet.new
  end

  def create
    @tweets = Tweet.all.sort_by(&:created_at).reverse
    @tweet = Tweet.new(tweet_params)

    if @tweet.save
      redirect_to tweets_path
    else
      render 'index'
    end
  end

  def destroy
    @tweet = Tweet.find(params[:id])
    @tweet.destroy

    redirect_to tweets_path
  end

  private
  def tweet_params
    params.require(:tweet).permit(:content)
  end
end
