#!/bin/sh
echo "Starting Ali Twitter..."
set -e
rm -f /alitwitter/tmp/pids/server.pid
bundle exec rake db:migrate
exec "$@"
