require 'rails_helper'

RSpec.describe 'Tweets', type: :routing do
  describe 'GET /' do
    it 'routes to the tweets index' do
      expect(get('/')).to route_to('tweets#index')
    end
  end

  describe 'GET /tweets' do
    it 'routes to the tweets index' do
      expect(get('/tweets')).to route_to('tweets#index')
    end
  end

  describe 'POST /tweets' do
    it 'routes to the tweets index' do
      expect(post('/tweets')).to route_to('tweets#create')
    end
  end

  describe 'DELETE /tweets' do
    it 'routes to the tweets destroy' do
      expect(delete('/tweets/1')).to route_to('tweets#destroy', id: '1')
    end
  end
end
