require 'rails_helper'

RSpec.describe 'Tweets', type: :request do
  describe 'GET /index' do
    it 'returns http success' do
      get '/tweets/index'
      expect(response).to have_http_status(:success)
    end
  end

  describe 'POST /' do
    context 'when given valid content' do
      it 'returns http found' do
        post '/tweets', params: { tweet: { content: 'New Tweet' } }
        expect(response).to have_http_status(:found)
      end
    end

    context 'when given invalid content' do
      it 'returns http success' do
        post '/tweets', params: { tweet: { content: '' } }
        expect(response).to have_http_status(:success)
      end
    end
  end
end
