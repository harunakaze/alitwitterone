require 'rails_helper'

RSpec.describe Tweet, type: :model do
  context 'given a valid content' do
    it 'should be valid' do
      tweet = Tweet.new
      tweet.content = 'I do not use Twitter'
      expect(tweet).to be_valid
    end
  end

  context 'given an empty content' do
    it 'should not be valid' do
      tweet = Tweet.new
      tweet.content = ''
      expect(tweet).to be_invalid
    end
  end

  context 'given more than 140 character content' do
    it 'should not be valid' do
      tweet = Tweet.new
      tweet.content = 'A' * 141
      expect(tweet).to be_invalid
    end
  end
end
