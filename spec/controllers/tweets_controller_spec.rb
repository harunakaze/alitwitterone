require 'rails_helper'

RSpec.describe TweetsController, type: :controller do
  describe 'GET tweets#index' do
    it 'show index page' do
      get :index
      expect(response).to render_template('index')
    end
  end

  describe 'POST tweets#create' do
    context 'when given valid content' do
      it 'should create a new tweet' do
        expect do
          post :create, params: { tweet: { content: 'New Tweet' } }
        end.to change(Tweet, :count).by(1)
      end
    end

    context 'when given invalid content' do
      it 'should render index' do
        post :create, params: { tweet: { content: '' } }
        expect(response).to render_template(:index)
      end
    end
  end

  describe 'DELETE tweets#destroy' do
    it 'delete that tweet' do
      expect do
        tweet = Tweet.create content: 'Delete This'
        delete :destroy, params: { id: tweet.to_param }
      end.to change(Tweet, :count).by(0)
    end
  end
end
