Rails.application.routes.draw do
  get 'tweets/index'
  resources :tweets, only: %i[index create destroy]
  root 'tweets#index'
end
